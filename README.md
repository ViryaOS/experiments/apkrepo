# apkrepo

This repository is used to generate `viryaos/apkrepo-*` docker images
containing `.apk` package repository.

In order to generate the docker image, following information is required.

1. Aports repo name (`<APKREPO_NAME>`)

2. Aports tree version (`<VERSION>`)

3. Apk package architectures (For example - aarch64, x86\_64) (`<ARCH1> | <ARCH1>.<ARCH2>`)

4. Git commit short SHA (`<SHORT_SHA>`)

If `HEAD` is the commit that we are generating the repository for, then
we can get the short SHA using the following command.

```
(inside the aports directory)

$ git rev-parse --short HEAD
```

Once we have the above information, verify that we have required `.apk`
packages.

```
$ cd apkrepo

apkrepo $ tree <APKREPO_NAME>/<VERSION>
```

To build the docker image

```
$ cd apkrepo

apkrepo $ docker build --squash \
        --build-arg overlay=<APKREPO_NAME> \
        --build-arg version=<VERSION> \
        -t viryaos/apkrepo-<APKREPO_NAME>:<VERSION>-<SHORT_SHA>-<ARCH1>.<ARCH2> .

(For example)

apkrepo $ docker build --squash \
        --build-arg overlay=sdk \
        --build-arg version=v3.7 \
        -t viryaos/apkrepo-sdk:v3.7-abcdef1-aarch64.x86_64 .

apkrepo $ docker push viryaos/apkrepo-sdk:v3.7-abcdef1-aarch64.x86_64
```

To extract repository from a docker image (using apkrepo-sdk as an example)

```
$ docker run -v /tmp:/tmp --rm -ti viryaos/apkrepo-sdk:v3.7-abcdef1-aarch64.x86_64

# cp -r /home/builder/apkrepo/sdk /tmp/apkrepo-sdk

apkrepo$ sudo mv /tmp/apkrepo-sdk sdk

apkrepo$ sudo chown -R 500:500 sdk
```
